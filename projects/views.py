from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.views.generic import (
#     ListView,
#     DetailView,
#     CreateView,
#     DeleteView,
#     UpdateView,
# )

# Create your views here.


@login_required
def projects_list_view(request):
    context = {
        "projects_list": Project.objects.filter(members=request.user),
    }
    return render(request, "projects/list.html", context)


@login_required
def project_details(request, pk):
    project = Project.objects.get(pk=pk)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", pk=project.pk)
            # return redirect("project_details", pk=project.pk)
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/new.html", context)
