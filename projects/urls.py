from django.urls import path
from projects.views import (
    projects_list_view,
    project_details,
    create_project,
)

urlpatterns = [
    path("", projects_list_view, name="list_projects"),
    path("<int:pk>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
